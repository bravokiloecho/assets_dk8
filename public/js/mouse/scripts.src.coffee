Elwyn = do ->

	defineDeviceVariables = ->

		deviceVariables =
			isMobile: if $('#mobileTester').css('visibility') == 'visible' then true else false
			isTablet: if $('#tabletTester').css('visibility') == 'visible' then true else false
			isTouch: if Modernizr.touch then true else false

		deviceVariables.isDevice = if deviceVariables.isMobile or deviceVariables.isTablet then true else false

		return deviceVariables
	
	# Returns a random float or integer between min (inclusive) and max (inclusive)
	getRandomNumber = ( min, max, integer ) ->
		
		if integer == false
			return Math.random() * ( max - min ) + min
		
		return Math.floor(Math.random() * (max - min + 1)) + min


	# SWAP SVGs with PNGs
	svgFallback = (svgSupport) ->
		if !svgSupport
			$('img[src*="svg"]').attr 'src', ->
				$(this).attr('src').replace('.svg', '.png')

	randomString = (length, chars) ->
		mask = ""
		mask += "abcdefghijklmnopqrstuvwxyz"  if chars.indexOf("a") > -1
		mask += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"  if chars.indexOf("A") > -1
		mask += "0123456789"  if chars.indexOf("#") > -1
		mask += "~`!@#$%^&*()_+-={}[]:\";'<>?,./|\\"  if chars.indexOf("!") > -1
		result = ""
		i = length

		while i > 0
			result += mask[Math.round(Math.random() * (mask.length - 1))]
			--i

		return result

	placeholder = ( els, activeClass = "active" ) ->
			$(els).each ->
				$el = $(this)
				$el.val $el.data().placeholder
				if $.trim($el.val()) is ""
					$el
						.val placeholder
						.removeClass activeClass
						.parent()
						.removeClass activeClass
				$el.focus(->
					placeholder = $el.data().placeholder
					console.log 'val: '+$el.val()
					console.log 'placeholder: '+placeholder
					if $el.val() is placeholder
						$el
							.val ""
							.addClass activeClass
							.parent()
							.addClass activeClass
				).blur ->
					placeholder = $el.data().placeholder
					if $.trim($el.val()) is ""
						$el
							.val placeholder
							.removeClass activeClass
							.parent()
							.removeClass activeClass

	trackEvent = (category,action,label = 'undefined',value = 0) ->
		
		if typeof ga == 'undefined'
			# console.log 'Track event: '
			# console.log ' category: '+category
			# console.log ' action: '+action
			# console.log ' label: '+label
			return

		ga(
			'send',
			'event',
			category,
			action,
			label,
			value
		)

	preloadImage = ( src ) ->
		img = new Image()
		img.src = src

	
	# http://stackoverflow.com/questions/5899783/detect-safari-chrome-ie-firefox-opera-with-user-agent
	browserDetect = ->
		browser = 
			chrome: navigator.userAgent.indexOf('Chrome') > -1
			explorer: navigator.userAgent.indexOf('MSIE') > -1
			firefox: navigator.userAgent.indexOf('Firefox') > -1
			safari: navigator.userAgent.indexOf("Safari") > -1
			opera: navigator.userAgent.toLowerCase().indexOf("op") > -1
		
		if browser.chrome and browser.safari
			browser.safari = false
		if browser.chrome and browser.opera 
			browser.chrome = false

		return browser


	# plugin for selecting text
	# from: http://is.gd/E1kgQ7
	jQuery.fn.selectText = ->
		doc = document
		element = this[0]
		range = undefined
		selection = undefined
		if doc.body.createTextRange
			range = document.body.createTextRange()
			range.moveToElementText element
			range.select()
		else if window.getSelection
			selection = window.getSelection()
			range = document.createRange()
			range.selectNodeContents element
			selection.removeAllRanges()
			selection.addRange range

	# plugin for setting opacity
	jQuery.fn.opacity = ( o ) ->
		@each ->
			$(this).css opacity: o

	# Degrees to radians
	degToRad = ( deg ) ->
		return deg * (Math.PI / 180)

	stripTrailingSlash = ( url ) ->
		if url.substr( url.length - 1 ) == '/'
			return url.substr(0, url.length - 1)
		return url
	


	# EXPORT
	{
		svgFallback: svgFallback
		randomString: randomString
		placeholder: placeholder
		trackEvent: trackEvent
		getRandomNumber: getRandomNumber
		preloadImage: preloadImage
		defineDeviceVariables: defineDeviceVariables
		browserDetect: browserDetect
		degToRad: degToRad
		stripTrailingSlash: stripTrailingSlash
	}
Global = do ->

	isMobile = null
	isTablet = null
	isDevice = null
	isTouch = null

	$html = null
	$body = null
	$wrapper = null
	
	init = ->
		# DEFINE GLOBAL VARIABLES HERE
		gv = window.gv =
			$window: $(window)
			$document: $(document)
			$html: $('html').first()
			$body: $('body').first()
			WIDTH: $(window).width()
			HEIGHT: $(window).height()
			deviceVariables: Elwyn.defineDeviceVariables()
			isDev: window.isDev
			$wrapper: $ '#wrapper'
			# browser: Elwyn.browserDetect()

		$html = gv.$html
		$body = gv.$body
		$wrapper = gv.$wrapper

		# Sitewide vars
		gv.siteURL = Elwyn.stripTrailingSlash window.siteURL

		isMobile = gv.deviceVariables.isMobile
		isTablet = gv.deviceVariables.isTablet
		isDevice = gv.deviceVariables.isDevice
		isTouch = gv.deviceVariables.isTouch

		# IS DEKSTOP?
		if !isMobile && !isTablet
			gv.isDesktop = true
		else
			gv.isDesktop = false
	
		# Get browser version
		browser = Elwyn.browserDetect()
		# console.log 'Browser version'
		# console.log browser

		do backToTopOnClick
		do logLinkClicks
		do testLegacyWarning
		do setDeviceBodyClasses

		# FASTCLICK
		FastClick.attach document.body


	logLinkClicks = ->
		$body.on 'click', 'a', ->
			href = $(this).attr 'href'
			Class = $(this).attr 'class'
			label = if href then href else Class
			# category, action, label
			Elwyn.trackEvent 'linkClick', 'click', label


	backToTopOnClick = ->
		$wrapper.on 'click', '.back-to-top', ->
			scrollTo 0

	toggleFullscreen = ->

		# Uses fullscreen plugin
		# https://github.com/private-face/jquery.fullscreen

		isFullscreen = $.fullscreen.isFullScreen()

		if !isFullscreen
			gv.$body.fullscreen()
		else
			$.fullscreen.exit()

	scrollTo = ( offset, speed, $target ) ->
		
		# Uses velocity.js

		offset = offset || 0
		speed = speed || 200
		$target = $target || $ 'html'

		$target.velocity 'scroll',
			duration: speed
			offset: offset
			easing: 'ease-out'


	setDeviceBodyClasses = ->
		
		if isMobile
			$body
			.addClass 'is-mobile'
			.addClass 'is-device'
			return

		if isTablet
			$body
			.addClass 'is-tablet'
			.addClass 'is-device'
			return


	testLegacyWarning = ->
		$html = $('html').first()
		ieTest = $html.attr('data-ie')
		
		if ieTest is 'ie7'
			$html.addClass 'ie7'
		else if ieTest is 'ie8'
			$html.addClass 'ie8'
		else if ieTest is 'ie9'
			$html.addClass 'ie9'

	{
		init: init
		toggleFullscreen: toggleFullscreen
		scrollTo: scrollTo
	}	
Load = do ->

	# LOAD A GALLERY PAGE
	page = ( url, promise ) ->
		$.ajax url,
			cache: true
			dataType: 'html'
			success: ( markup ) ->
				$content = $(markup)
				promise.resolve( $content )
			error: ->
				# If fail, just redirect to the correct page
				window.location.href = url

	{
		page: page
	}
Main = do ->
	
	# GLOBAL
	$window = null
	$body = null
	$wrapper = null
	WIDTH = null
	HEIGHT = null
	isMobile = null
	isTablet = null
	isDevice = null
	isTouch = null

	# SPECIFIC TO HERE


	defineVars = ->
		gv = window.gv
		$window = gv.$window
		$body = gv.$body
		$wrapper = gv.$wrapper
		WIDTH = gv.WIDTH
		HEIGHT = gv.HEIGHT

		isMobile = gv.deviceVariables.isMobile
		isTablet = gv.deviceVariables.isTablet
		isDevice = gv.deviceVariables.isDevice
		isTouch = gv.deviceVariables.isTouch

		# SPECIFIC TO HERE


	init = ->
		do defineVars

		if window.isDev
			do keyboardShortcuts
		

	
	# MISC FUNCTIONS
	# ##############

	keyboardShortcuts = ( $slider, $imagePanel ) ->

		if $.fullscreen.isNativelySupported()
			Mousetrap.bind [ 'f' ] , (e) ->
				do SITE.toggleFullscreen
				return

		Mousetrap.bind [ 't' ] , (e) ->
			do toggleHomeThumbs

		Mousetrap.bind [ 'l' ] , (e) ->
			do testLoad



	{
		init: init
	}
PageTransition = do ->

    # GLOBAL
    $window = null
    $body = null
    $wrapper = null

    # SPECIFIC TO HERE
    siteURL = null
    currentPage = null
    
    homePage = null
    aboutPage = null
    galleriesParent = null
    galleryPage = null

    transitionInProgress = false
    pathToRun = false

    # TYPE OF GALLERY
    isDesktop = null
    
    defineVars = ->
        gv = window.gv
        $window = gv.$window
        $body = gv.$body
        $wrapper = gv.$wrapper

        # Specific to here
        siteURL = gv.siteURL
        currentPage = window.currentPage
        currentSlug = window.currentSlug

        homePage = 'home'
        aboutPage = window.pathNames.about
        galleriesParent = window.pathNames.galleriesParent
        galleryPage = 'gallery'

        # TYPE OF GALLERY
        isDesktop = gv.isDesktop

    init = ->
        do defineVars

    # INITIAL SETUP
    # Run this to setup initial page
    initialSetup = ( initialPage, slug, promise ) ->
        console.log 'initialSetup'
        console.log 'Page in: ' + initialPage
        console.log 'Slug: ' + slug

        pageReady = $.Deferred()

        # HOME PAGE
        if initialPage is homePage
            pageReady.resolve()
            $.when( pageReady ).then ->
                promise.resolve()

        # OTHER PAGES
        else

            # INITIAL GALLERY SETUP
            if initialPage is galleryPage

                # Do somethin then resolve pageReady promise

                $.when( pageReady ).then ->
                    promise.resolve()

                return


    # ROUTES
    # The routes are controlled here

    # HOME => GALLERY
    homeToGallery = ( slug, promise ) ->
        url = siteURL + '/' + galleriesParent + '/' + slug
        galleryLoaded = $.Deferred()
        galleryReady = $.Deferred()


        # Load gallery markup
        Load.page url, galleryLoaded
        $.when( galleryLoaded ).then ( $markup ) ->
            startGallery $markup, galleryReady

        $.when( galleryReady ).then ->
            # Do something
            promise.resolve()

    # GALLERY => HOME
    galleryToHome = ( promise ) ->
        galleryHidden = $.Deferred()

        # Do something and resolve promise

        $.when( galleryHidden ).then ->
            promise.resolve()


    # HELPERS
    # #######
            


    # EXPORT FUNCTIONS
    {
        init: init
        initialSetup: initialSetup
        homeToGallery: homeToGallery
        galleryToHome: galleryToHome
    }

Routing = do ->

    # GLOBAL
    $window = null
    $body = null
    $wrapper = null

    # SPECIFIC TO HERE
    initialPage = true
    currentPage = null
    
    homePage = null
    aboutPage = null
    galleriesParent = null
    galleryPage = null

    # Defind in setupRouting
    homePath = null
    galleryPath = null
    aboutPath = null

    transitionInProgress = false
    pathToRun = false
    
    defineVars = ->
        gv = window.gv
        $window = gv.$window
        $body = gv.$body
        $wrapper = gv.$wrapper

        # Specific to here
        
        currentPage = ''
        currentSlug = window.currentSlug

        homePage = 'home'
        aboutPage = window.pathNames.about
        galleriesParent = window.pathNames.galleriesParent
        galleryPage = 'gallery'


    init = ->

        if !Modernizr.history
            return

        do defineVars
        do setupRouting
        

    # ROUTING USES DAVIS.JS
    setupRouting = ->
        
        homePath = '/?'
        galleryPath = '/' + galleriesParent
        aboutPath = '/' + aboutPage + '/?'

        # ROUTES
        app = Davis ->
            
            # HOME
            this.get homePath, ( r ) ->
                
                pageIn = homePage
                pageOut = currentPage
                
                # If transition in progress, store path and run at end
                if transitionInProgress
                    cachePath r.path
                    return

                # if same page, do nothing
                if pageIn is pageOut
                    console.log 'pageIn: '+pageIn
                    console.log 'pageOut: '+pageOut
                    console.log '*** SAME PAGE, STOP HERE ***********'
                    return

                switchPage pageOut, pageIn

            # ABOUT
            this.get aboutPath, ( r ) ->
                
                pageIn = aboutPage
                pageOut = currentPage
                
                # If transition in progress, store path and run at end
                if transitionInProgress
                    cachePath r.path
                    return

                # if same page, do nothing
                if pageIn is pageOut
                    console.log '*** SAME PAGE, STOP HERE ***********'
                    return

                args = 
                    requiresAjax: true

                switchPage pageOut, pageIn

            # GALLERY PAGE
            this.scope galleryPath, ( r ) ->

                # If transition in progress, store path and run at end
                if transitionInProgress
                    console.log 'TRANSITION IN PROGRESS'
                    cachePath r.path
                    return

                # Gallery without hash
                this.get '/:slug', ( r ) ->

                    pageIn = galleryPage
                    pageOut = currentPage
                    slug = r.params.slug
                    
                    # if same page, do nothing
                    if pageIn is pageOut and slug is currentSlug
                        console.log '*** SAME PAGE, STOP HERE ***********'
                        console.log 'pageIn: '+pageIn
                        console.log 'slugIn: '+slug
                        console.log 'pageOut: '+pageOut
                        console.log 'currentSlug: '+currentSlug
                        return

                    args = 
                        requiresAjax: true
                        pageType: pageIn
                        slug: slug

                    switchPage pageOut, pageIn, args

                # Gallery with hash
                this.get '/:slug/*splash', ( r ) ->

                    pageIn = galleryPage
                    pageOut = currentPage
                    slug = r.params.slug
                    hash = r.params.splash.replace '#/', ''
                    
                    # if same page, do nothing
                    if pageIn is pageOut and slug is currentSlug
                        console.log '*** SAME PAGE, STOP HERE ***********'
                        console.log 'pageIn: '+pageIn
                        console.log 'slugIn: '+slug
                        console.log 'pageOut: '+pageOut
                        console.log 'currentSlug: '+currentSlug
                        return

                    args = 
                        requiresAjax: true
                        pageType: pageIn
                        slug: slug

                    switchPage pageOut, pageIn, args


        app.configure ( config ) ->
            config.linkSelector = '.ajax'
            config.generateRequestOnPageLoad = true

        app.start()


    switchPage = ( pageOut, pageIn, args ) ->

        args = args || {}

        slug = args.slug
        requiresAjax = args.requiresAjax

        transitionInProgress = true
        transitionPromise = $.Deferred()

        console.log '*** RUN PATH ***'
        console.log 'pageOut: '+pageOut
        console.log 'pageIn: '+pageIn
        console.log 'transitionInProgress: '+transitionInProgress

        # Run transition
        runTransitionFunction pageOut, pageIn, slug, transitionPromise

        # At end of page transition, check whether
        # there are any pending paths
        # if so, run that path
        $.when( transitionPromise ).then ->
            
            if pathToRun
                console.log '!!!!!!! THERE IS A PATH TO RUN !!!!!!!'
                console.log 'PATH: ' + pathToRun
                
                # Set some variable before recalling the page transition
                transitionInProgress = false
                setFinalPageVars pageIn, slug
                
                runPath pathToRun

                # Reset path to run
                pathToRun = false
                return

            # At end, set vars
            console.log 'END OF PAGE TRANSITION'
            pathToRun = false
            transitionInProgress = false
            setFinalPageVars pageIn, slug

    runTransitionFunction = ( pageOut, pageIn, slug, promise ) ->

        # FOR INITIAL PAGE LOAD
        if initialPage
            PageTransition.initialSetup pageIn, slug, promise
            initialPage = false
            return
        
        # HOME => GALLERY
        if pageOut is homePage and pageIn is galleryPage
            console.log 'HOME => GALLERY' 
            PageTransition.homeToGallery slug, promise
            return

        # GALLERY => HOME
        if pageOut is galleryPage and pageIn is homePage
            console.log 'GALLERY => HOME' 
            PageTransition.galleryToHome promise
            return

        # Sub pages to sub pages...
        # #########################

        pagesInvolved = {}

        pagesInvolved.out = pageOut
        pagesInvolved.in = pageIn

        # PageTransition.subPagetoSubPage promise, pagesInvolved, slug

    # MISC ROUTING FUNCTIONS
    setFinalPageVars = ( pageIn, slug, transitionComplete = true ) ->
        
        currentPage = window.currentPage = pageIn
        currentSlug = window.currentSlug = slug

        $body
            .data 'page', currentPage
            .removeClass()
            .addClass currentPage

    cachePath = ( path ) ->
        pathToRun = path

    runPath = ( path ) ->
        Davis.location.assign new Davis.Request path

    goHome = ->
        runPath homePath.replace '?', ''

    # Export functions
    {
        init: init
        goHome: goHome
    }
# toggleNavElementsOnMousemove = ->
# 	delay = 3000
# 	showing = true

# 	timer = setTimeout(->
# 		if $homepageNavElements.filter('.hovered').length || $projectLinksContainer.hasClass 'open'
# 			return
# 		$homepageNavElements.addClass 'hidden'
# 		showing = false
# 	, delay)

# 	$body.on 'mousemove', ->
# 		if timer
# 			clearTimeout timer
# 			timer = 0

# 		if !showing
# 			$homepageNavElements.removeClass 'hidden'

# 		showing = true

# 		timer = setTimeout(->
# 			if $homepageNavElements.filter('.hovered').length || $projectLinksContainer.hasClass 'open'
# 				return
# 			$homepageNavElements.addClass 'hidden'
# 			showing = false
# 		, delay)


# setHoverClassOnHomeElements = ->
# 	$body.on 'mouseenter', '.homepage-nav-element', ->
# 		$(this).addClass 'hovered'
# 	$body.on 'mouseleave', '.homepage-nav-element', ->
# 		$(this).removeClass 'hovered'


# toggleTopInstructions = ( type, switchTo ) ->

# 	$all = $topInstructions.children()

# 	if !type
# 		$all.hide()
# 		return
	
# 	$text = $topInstructions.children '#' + type + '-instructions'

# 	# TURN ON
# 	if switchTo # == 1
# 		$text.show()
# 		return
# 	# TURN OFF
# 	else
# 		$text.hide()
$ ->

	do Global.init

	do Main.init

	do Gallery.init

	do PageTransition.init

	# console.log 'ROUTING NOT INIT'
	do Routing.init