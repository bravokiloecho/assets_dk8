var Elwyn, Global, Load, Main, PageTransition, Routing;

Elwyn = (function() {
  var browserDetect, defineDeviceVariables, degToRad, getRandomNumber, placeholder, preloadImage, randomString, stripTrailingSlash, svgFallback, trackEvent;
  defineDeviceVariables = function() {
    var deviceVariables;
    deviceVariables = {
      isMobile: $('#mobileTester').css('visibility') === 'visible' ? true : false,
      isTablet: $('#tabletTester').css('visibility') === 'visible' ? true : false,
      isTouch: Modernizr.touch ? true : false
    };
    deviceVariables.isDevice = deviceVariables.isMobile || deviceVariables.isTablet ? true : false;
    return deviceVariables;
  };
  getRandomNumber = function(min, max, integer) {
    if (integer === false) {
      return Math.random() * (max - min) + min;
    }
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };
  svgFallback = function(svgSupport) {
    if (!svgSupport) {
      return $('img[src*="svg"]').attr('src', function() {
        return $(this).attr('src').replace('.svg', '.png');
      });
    }
  };
  randomString = function(length, chars) {
    var i, mask, result;
    mask = "";
    if (chars.indexOf("a") > -1) {
      mask += "abcdefghijklmnopqrstuvwxyz";
    }
    if (chars.indexOf("A") > -1) {
      mask += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }
    if (chars.indexOf("#") > -1) {
      mask += "0123456789";
    }
    if (chars.indexOf("!") > -1) {
      mask += "~`!@#$%^&*()_+-={}[]:\";'<>?,./|\\";
    }
    result = "";
    i = length;
    while (i > 0) {
      result += mask[Math.round(Math.random() * (mask.length - 1))];
      --i;
    }
    return result;
  };
  placeholder = function(els, activeClass) {
    if (activeClass == null) {
      activeClass = "active";
    }
    return $(els).each(function() {
      var $el;
      $el = $(this);
      $el.val($el.data().placeholder);
      if ($.trim($el.val()) === "") {
        $el.val(placeholder).removeClass(activeClass).parent().removeClass(activeClass);
      }
      return $el.focus(function() {
        placeholder = $el.data().placeholder;
        console.log('val: ' + $el.val());
        console.log('placeholder: ' + placeholder);
        if ($el.val() === placeholder) {
          return $el.val("").addClass(activeClass).parent().addClass(activeClass);
        }
      }).blur(function() {
        placeholder = $el.data().placeholder;
        if ($.trim($el.val()) === "") {
          return $el.val(placeholder).removeClass(activeClass).parent().removeClass(activeClass);
        }
      });
    });
  };
  trackEvent = function(category, action, label, value) {
    if (label == null) {
      label = 'undefined';
    }
    if (value == null) {
      value = 0;
    }
    if (typeof ga === 'undefined') {
      return;
    }
    return ga('send', 'event', category, action, label, value);
  };
  preloadImage = function(src) {
    var img;
    img = new Image();
    return img.src = src;
  };
  browserDetect = function() {
    var browser;
    browser = {
      chrome: navigator.userAgent.indexOf('Chrome') > -1,
      explorer: navigator.userAgent.indexOf('MSIE') > -1,
      firefox: navigator.userAgent.indexOf('Firefox') > -1,
      safari: navigator.userAgent.indexOf("Safari") > -1,
      opera: navigator.userAgent.toLowerCase().indexOf("op") > -1
    };
    if (browser.chrome && browser.safari) {
      browser.safari = false;
    }
    if (browser.chrome && browser.opera) {
      browser.chrome = false;
    }
    return browser;
  };
  jQuery.fn.selectText = function() {
    var doc, element, range, selection;
    doc = document;
    element = this[0];
    range = void 0;
    selection = void 0;
    if (doc.body.createTextRange) {
      range = document.body.createTextRange();
      range.moveToElementText(element);
      return range.select();
    } else if (window.getSelection) {
      selection = window.getSelection();
      range = document.createRange();
      range.selectNodeContents(element);
      selection.removeAllRanges();
      return selection.addRange(range);
    }
  };
  jQuery.fn.opacity = function(o) {
    return this.each(function() {
      return $(this).css({
        opacity: o
      });
    });
  };
  degToRad = function(deg) {
    return deg * (Math.PI / 180);
  };
  stripTrailingSlash = function(url) {
    if (url.substr(url.length - 1) === '/') {
      return url.substr(0, url.length - 1);
    }
    return url;
  };
  return {
    svgFallback: svgFallback,
    randomString: randomString,
    placeholder: placeholder,
    trackEvent: trackEvent,
    getRandomNumber: getRandomNumber,
    preloadImage: preloadImage,
    defineDeviceVariables: defineDeviceVariables,
    browserDetect: browserDetect,
    degToRad: degToRad,
    stripTrailingSlash: stripTrailingSlash
  };
})();

Global = (function() {
  var $body, $html, $wrapper, backToTopOnClick, init, isDevice, isMobile, isTablet, isTouch, logLinkClicks, scrollTo, setDeviceBodyClasses, testLegacyWarning, toggleFullscreen;
  isMobile = null;
  isTablet = null;
  isDevice = null;
  isTouch = null;
  $html = null;
  $body = null;
  $wrapper = null;
  init = function() {
    var browser, gv;
    gv = window.gv = {
      $window: $(window),
      $document: $(document),
      $html: $('html').first(),
      $body: $('body').first(),
      WIDTH: $(window).width(),
      HEIGHT: $(window).height(),
      deviceVariables: Elwyn.defineDeviceVariables(),
      isDev: window.isDev,
      $wrapper: $('#wrapper')
    };
    $html = gv.$html;
    $body = gv.$body;
    $wrapper = gv.$wrapper;
    gv.siteURL = Elwyn.stripTrailingSlash(window.siteURL);
    isMobile = gv.deviceVariables.isMobile;
    isTablet = gv.deviceVariables.isTablet;
    isDevice = gv.deviceVariables.isDevice;
    isTouch = gv.deviceVariables.isTouch;
    if (!isMobile && !isTablet) {
      gv.isDesktop = true;
    } else {
      gv.isDesktop = false;
    }
    browser = Elwyn.browserDetect();
    backToTopOnClick();
    logLinkClicks();
    testLegacyWarning();
    setDeviceBodyClasses();
    return FastClick.attach(document.body);
  };
  logLinkClicks = function() {
    return $body.on('click', 'a', function() {
      var Class, href, label;
      href = $(this).attr('href');
      Class = $(this).attr('class');
      label = href ? href : Class;
      return Elwyn.trackEvent('linkClick', 'click', label);
    });
  };
  backToTopOnClick = function() {
    return $wrapper.on('click', '.back-to-top', function() {
      return scrollTo(0);
    });
  };
  toggleFullscreen = function() {
    var isFullscreen;
    isFullscreen = $.fullscreen.isFullScreen();
    if (!isFullscreen) {
      return gv.$body.fullscreen();
    } else {
      return $.fullscreen.exit();
    }
  };
  scrollTo = function(offset, speed, $target) {
    offset = offset || 0;
    speed = speed || 200;
    $target = $target || $('html');
    return $target.velocity('scroll', {
      duration: speed,
      offset: offset,
      easing: 'ease-out'
    });
  };
  setDeviceBodyClasses = function() {
    if (isMobile) {
      $body.addClass('is-mobile').addClass('is-device');
      return;
    }
    if (isTablet) {
      $body.addClass('is-tablet').addClass('is-device');
    }
  };
  testLegacyWarning = function() {
    var ieTest;
    $html = $('html').first();
    ieTest = $html.attr('data-ie');
    if (ieTest === 'ie7') {
      return $html.addClass('ie7');
    } else if (ieTest === 'ie8') {
      return $html.addClass('ie8');
    } else if (ieTest === 'ie9') {
      return $html.addClass('ie9');
    }
  };
  return {
    init: init,
    toggleFullscreen: toggleFullscreen,
    scrollTo: scrollTo
  };
})();

Load = (function() {
  var page;
  page = function(url, promise) {
    return $.ajax(url, {
      cache: true,
      dataType: 'html',
      success: function(markup) {
        var $content;
        $content = $(markup);
        return promise.resolve($content);
      },
      error: function() {
        return window.location.href = url;
      }
    });
  };
  return {
    page: page
  };
})();

Main = (function() {
  var $body, $window, $wrapper, HEIGHT, WIDTH, defineVars, init, isDevice, isMobile, isTablet, isTouch, keyboardShortcuts;
  $window = null;
  $body = null;
  $wrapper = null;
  WIDTH = null;
  HEIGHT = null;
  isMobile = null;
  isTablet = null;
  isDevice = null;
  isTouch = null;
  defineVars = function() {
    var gv;
    gv = window.gv;
    $window = gv.$window;
    $body = gv.$body;
    $wrapper = gv.$wrapper;
    WIDTH = gv.WIDTH;
    HEIGHT = gv.HEIGHT;
    isMobile = gv.deviceVariables.isMobile;
    isTablet = gv.deviceVariables.isTablet;
    isDevice = gv.deviceVariables.isDevice;
    return isTouch = gv.deviceVariables.isTouch;
  };
  init = function() {
    defineVars();
    if (window.isDev) {
      return keyboardShortcuts();
    }
  };
  keyboardShortcuts = function($slider, $imagePanel) {
    if ($.fullscreen.isNativelySupported()) {
      Mousetrap.bind(['f'], function(e) {
        SITE.toggleFullscreen();
      });
    }
    Mousetrap.bind(['t'], function(e) {
      return toggleHomeThumbs();
    });
    return Mousetrap.bind(['l'], function(e) {
      return testLoad();
    });
  };
  return {
    init: init
  };
})();

PageTransition = (function() {
  var $body, $window, $wrapper, aboutPage, currentPage, defineVars, galleriesParent, galleryPage, galleryToHome, homePage, homeToGallery, init, initialSetup, isDesktop, pathToRun, siteURL, transitionInProgress;
  $window = null;
  $body = null;
  $wrapper = null;
  siteURL = null;
  currentPage = null;
  homePage = null;
  aboutPage = null;
  galleriesParent = null;
  galleryPage = null;
  transitionInProgress = false;
  pathToRun = false;
  isDesktop = null;
  defineVars = function() {
    var currentSlug, gv;
    gv = window.gv;
    $window = gv.$window;
    $body = gv.$body;
    $wrapper = gv.$wrapper;
    siteURL = gv.siteURL;
    currentPage = window.currentPage;
    currentSlug = window.currentSlug;
    homePage = 'home';
    aboutPage = window.pathNames.about;
    galleriesParent = window.pathNames.galleriesParent;
    galleryPage = 'gallery';
    return isDesktop = gv.isDesktop;
  };
  init = function() {
    return defineVars();
  };
  initialSetup = function(initialPage, slug, promise) {
    var pageReady;
    console.log('initialSetup');
    console.log('Page in: ' + initialPage);
    console.log('Slug: ' + slug);
    pageReady = $.Deferred();
    if (initialPage === homePage) {
      pageReady.resolve();
      return $.when(pageReady).then(function() {
        return promise.resolve();
      });
    } else {
      if (initialPage === galleryPage) {
        $.when(pageReady).then(function() {
          return promise.resolve();
        });
      }
    }
  };
  homeToGallery = function(slug, promise) {
    var galleryLoaded, galleryReady, url;
    url = siteURL + '/' + galleriesParent + '/' + slug;
    galleryLoaded = $.Deferred();
    galleryReady = $.Deferred();
    Load.page(url, galleryLoaded);
    $.when(galleryLoaded).then(function($markup) {
      return startGallery($markup, galleryReady);
    });
    return $.when(galleryReady).then(function() {
      return promise.resolve();
    });
  };
  galleryToHome = function(promise) {
    var galleryHidden;
    galleryHidden = $.Deferred();
    return $.when(galleryHidden).then(function() {
      return promise.resolve();
    });
  };
  return {
    init: init,
    initialSetup: initialSetup,
    homeToGallery: homeToGallery,
    galleryToHome: galleryToHome
  };
})();

Routing = (function() {
  var $body, $window, $wrapper, aboutPage, aboutPath, cachePath, currentPage, defineVars, galleriesParent, galleryPage, galleryPath, goHome, homePage, homePath, init, initialPage, pathToRun, runPath, runTransitionFunction, setFinalPageVars, setupRouting, switchPage, transitionInProgress;
  $window = null;
  $body = null;
  $wrapper = null;
  initialPage = true;
  currentPage = null;
  homePage = null;
  aboutPage = null;
  galleriesParent = null;
  galleryPage = null;
  homePath = null;
  galleryPath = null;
  aboutPath = null;
  transitionInProgress = false;
  pathToRun = false;
  defineVars = function() {
    var currentSlug, gv;
    gv = window.gv;
    $window = gv.$window;
    $body = gv.$body;
    $wrapper = gv.$wrapper;
    currentPage = '';
    currentSlug = window.currentSlug;
    homePage = 'home';
    aboutPage = window.pathNames.about;
    galleriesParent = window.pathNames.galleriesParent;
    return galleryPage = 'gallery';
  };
  init = function() {
    if (!Modernizr.history) {
      return;
    }
    defineVars();
    return setupRouting();
  };
  setupRouting = function() {
    var app;
    homePath = '/?';
    galleryPath = '/' + galleriesParent;
    aboutPath = '/' + aboutPage + '/?';
    app = Davis(function() {
      this.get(homePath, function(r) {
        var pageIn, pageOut;
        pageIn = homePage;
        pageOut = currentPage;
        if (transitionInProgress) {
          cachePath(r.path);
          return;
        }
        if (pageIn === pageOut) {
          console.log('pageIn: ' + pageIn);
          console.log('pageOut: ' + pageOut);
          console.log('*** SAME PAGE, STOP HERE ***********');
          return;
        }
        return switchPage(pageOut, pageIn);
      });
      this.get(aboutPath, function(r) {
        var args, pageIn, pageOut;
        pageIn = aboutPage;
        pageOut = currentPage;
        if (transitionInProgress) {
          cachePath(r.path);
          return;
        }
        if (pageIn === pageOut) {
          console.log('*** SAME PAGE, STOP HERE ***********');
          return;
        }
        args = {
          requiresAjax: true
        };
        return switchPage(pageOut, pageIn);
      });
      return this.scope(galleryPath, function(r) {
        if (transitionInProgress) {
          console.log('TRANSITION IN PROGRESS');
          cachePath(r.path);
          return;
        }
        this.get('/:slug', function(r) {
          var args, pageIn, pageOut, slug;
          pageIn = galleryPage;
          pageOut = currentPage;
          slug = r.params.slug;
          if (pageIn === pageOut && slug === currentSlug) {
            console.log('*** SAME PAGE, STOP HERE ***********');
            console.log('pageIn: ' + pageIn);
            console.log('slugIn: ' + slug);
            console.log('pageOut: ' + pageOut);
            console.log('currentSlug: ' + currentSlug);
            return;
          }
          args = {
            requiresAjax: true,
            pageType: pageIn,
            slug: slug
          };
          return switchPage(pageOut, pageIn, args);
        });
        return this.get('/:slug/*splash', function(r) {
          var args, hash, pageIn, pageOut, slug;
          pageIn = galleryPage;
          pageOut = currentPage;
          slug = r.params.slug;
          hash = r.params.splash.replace('#/', '');
          if (pageIn === pageOut && slug === currentSlug) {
            console.log('*** SAME PAGE, STOP HERE ***********');
            console.log('pageIn: ' + pageIn);
            console.log('slugIn: ' + slug);
            console.log('pageOut: ' + pageOut);
            console.log('currentSlug: ' + currentSlug);
            return;
          }
          args = {
            requiresAjax: true,
            pageType: pageIn,
            slug: slug
          };
          return switchPage(pageOut, pageIn, args);
        });
      });
    });
    app.configure(function(config) {
      config.linkSelector = '.ajax';
      return config.generateRequestOnPageLoad = true;
    });
    return app.start();
  };
  switchPage = function(pageOut, pageIn, args) {
    var requiresAjax, slug, transitionPromise;
    args = args || {};
    slug = args.slug;
    requiresAjax = args.requiresAjax;
    transitionInProgress = true;
    transitionPromise = $.Deferred();
    console.log('*** RUN PATH ***');
    console.log('pageOut: ' + pageOut);
    console.log('pageIn: ' + pageIn);
    console.log('transitionInProgress: ' + transitionInProgress);
    runTransitionFunction(pageOut, pageIn, slug, transitionPromise);
    return $.when(transitionPromise).then(function() {
      if (pathToRun) {
        console.log('!!!!!!! THERE IS A PATH TO RUN !!!!!!!');
        console.log('PATH: ' + pathToRun);
        transitionInProgress = false;
        setFinalPageVars(pageIn, slug);
        runPath(pathToRun);
        pathToRun = false;
        return;
      }
      console.log('END OF PAGE TRANSITION');
      pathToRun = false;
      transitionInProgress = false;
      return setFinalPageVars(pageIn, slug);
    });
  };
  runTransitionFunction = function(pageOut, pageIn, slug, promise) {
    var pagesInvolved;
    if (initialPage) {
      PageTransition.initialSetup(pageIn, slug, promise);
      initialPage = false;
      return;
    }
    if (pageOut === homePage && pageIn === galleryPage) {
      console.log('HOME => GALLERY');
      PageTransition.homeToGallery(slug, promise);
      return;
    }
    if (pageOut === galleryPage && pageIn === homePage) {
      console.log('GALLERY => HOME');
      PageTransition.galleryToHome(promise);
      return;
    }
    pagesInvolved = {};
    pagesInvolved.out = pageOut;
    return pagesInvolved["in"] = pageIn;
  };
  setFinalPageVars = function(pageIn, slug, transitionComplete) {
    var currentSlug;
    if (transitionComplete == null) {
      transitionComplete = true;
    }
    currentPage = window.currentPage = pageIn;
    currentSlug = window.currentSlug = slug;
    return $body.data('page', currentPage).removeClass().addClass(currentPage);
  };
  cachePath = function(path) {
    return pathToRun = path;
  };
  runPath = function(path) {
    return Davis.location.assign(new Davis.Request(path));
  };
  goHome = function() {
    return runPath(homePath.replace('?', ''));
  };
  return {
    init: init,
    goHome: goHome
  };
})();

$(function() {
  Global.init();
  Main.init();
  Gallery.init();
  PageTransition.init();
  return Routing.init();
});

//# sourceMappingURL=scripts.js.map
