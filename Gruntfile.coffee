module.exports = (grunt) ->
  
  # Project configuration.
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")

    asset_path: 'devAssets/'
    public_path: 'public/'
    view_path: 'views/'
    data_path: 'data/'
    # port: '4000' # uncomment for assemble sites

    assemble:
      options:
        assets: "<%= public_path %>"
        engine: 'jade'
        data: ['<%= data_path %>*.yml','<%= data_path %>*.json']
        flatten: false  
        plugins: [ 'assemble-markdown-import' ]
        basedir: 'views/'
      
        
      dev:
        options:
          production: false
          prettify: true
        expand: true
        cwd: "views/"
        src: [
          '*.jade'
          '*/*.jade'
          '*/*/*.jade'
          '!_partials/*'
        ]
        dest: 'dev/'
      
      public:
        options:
          production: true
          prettify: false
        expand: true
        cwd: "views/"
        src: [
          '*.jade'
          '*/*.jade'
          '*/*/*.jade'
          '!_partials/*'
        ]
        dest: 'public/'

    
    watch:
      sass:
        files: [
          "<%= asset_path %>sass/*"
          "<%= asset_path %>sass/*/*"
        ]
        tasks: ["compass:dev"]
        options:
          atBegin: false

      coffee:
        files: [
          "<%= asset_path %>coffee/*"
        ]
        tasks: ["coffee"]
        options:
          atBegin: false

      js:
        files: [
          "<%= asset_path %>js/*"
        ]
        tasks: ["uglify:js_scripts"]
        options:
          atBegin: false

      plugins:
        files: [
          "<%= asset_path %>plugins/*"
        ]
        tasks: ["uglify:plugins"]
        options:
          atBegin: false

      svg:
        files: [
          "<%= asset_path %>svg/*.svg"
          "<%= asset_path %>svg/*/*.svg"
        ]
        tasks: ["svgmin"]
        options:
          atBegin: false


      assemble:
        files: [
          "<%= view_path %>*.jade"
          "<%= view_path %>*/*.jade"
          "<%= view_path %>*/*/*.jade"
          "<%= data_path %>*"
        ]
        tasks: ["assemble:dev"]
        options:
          atBegin: false


      watchFiles:
        files: [
          "<%= public_path %>js/*"
          "<%= public_path %>js/mouse/*"
          "dev/*"
          "dev/*/*"
          "site/templates/*"
          "site/templates/*/*"
        ]
        options:
          livereload: true

    browserSync:
      options:
        watchTask: true
        port: "<%= port %>"

      files:
        src: [
          "<%= public_path %>css/mouse/*.css"
          "<%= public_path %>gfx/*"
          "<%= public_path %>gfx/*/*"
          "<%= public_path %>images/*"
        ]

    uglify:
      plugins:
        options:
          mangle: true
          beautify: false
          compress: true

        files:
          "<%= public_path %>js/plugins.js": ["<%= asset_path %>plugins/*.js"]

      js_scripts:
        options:
          mangle: false
          beautify: true
          compress: false
          sourceMap: true
        files:
          "<%= public_path %>js/mouse/js_scripts.js": ["<%= asset_path %>js/*.js"]

      
      # combines plugins and scripts
      combine_all:
        options:
          mangle: true
          beautify: false
          compress:
            drop_console: true

        files:
          "<%= public_path %>js/min/scripts.js": [
            "<%= public_path %>js/plugins.js"
            "<%= public_path %>js/mouse/scripts.js"
            "<%= public_path %>js/mouse/js_scripts.js"
          ]

    compass:
      dev:
        options:
          sassDir: "<%= asset_path %>sass"
          cssDir: "<%= public_path %>css/mouse"
          outputStyle: "nested"
          fontsPath: "<%= public_path %>Fonts"
          sourcemap: true

      dist:
        options:
          sassDir: "<%= asset_path %>sass"
          cssDir: "<%= public_path %>css/min"
          outputStyle: "compressed"
          fontsPath: "<%= public_path %>Fonts"
          sourcemap: false

    coffee:
      compileBare:
        options:
          bare: true
          join: false
          sourceMap: true

        files:
          "<%= public_path %>js/mouse/scripts.js": ["<%= asset_path %>coffee/*.coffee"]



    connect:
      server:
        options:
          port: "<%= port %>"
          hostname: "*"


    imagemin: # Task
      dynamic: # Another target
        options:
          cache: false
          optimizationLevel: 2

        files: [
          expand: true # Enable dynamic expansion
          cwd: "<%= asset_path %>photos/" # Src matches are relative to this path
          src: ["*.{png,jpg,gif}"] # Actual patterns to match
          dest: "<%= public_path %>photos/" # Destination path prefix
        ]

    image_resize:
      resize:
        options:
          overwrite: true
          upscale: false
          crop: false
          width: 1600
        src: '<%= asset_path %>photos/*.{png,jpg,gif}'
        dest: '<%= public_path %>photos/'

    svgmin:
      options:
        plugins: [
          {
            removeViewBox: false
          }, {
            removeUselessStrokeAndFill: true
          }
        ]
      dist:
        files: [
          expand: true # Enable dynamic expansion
          cwd: "<%= asset_path %>svg/" # Src matches are relative to this path
          src: ["*.svg","*/*.svg"] # Actual patterns to match
          dest: "<%= public_path %>gfx/" # Destination path prefix
        ]

    svg2png:
      all:
        files: [
          { # rasterize all SVG files in "img" and its subdirectories to "img/png"
            cwd: "<%= asset_path %>svg/"
            src: ["*.svg","*/*.svg"] # Actual patterns to match
            dest: "<%= public_path %>gfx/" # Destination path prefix
          }
        ]

    # DB DUMPING
    # ##########
    db_dumps: './db_dumps/'
    # path to DB dump config
    db_config: grunt.file.readJSON './db_dumps/dp_dump_config.json'

    db_dump:
      options: {}
      local:
        options:
          title: "Local DB"
          database: "<%= db_config.local.name %>"
          user: "<%= db_config.local.user %>"
          pass: "<%= db_config.local.pass %>"
          host: "<%= db_config.local.host %>"
          port: "<%= db_config.local.port %>"
          backup_to: "db_dumps/backups/benjaminelwyn-LOCAL-" + grunt.template.today("dd-mm-yyyy") + ".sql"
      staging:
        options:
          title: "Staging DB"
          database: "<%= db_config.staging.name %>"
          user: "<%= db_config.staging.user %>"
          pass: "<%= db_config.staging.pass %>"
          host: "<%= db_config.staging.host %>"
          port: "<%= db_config.staging.port %>"
          backup_to: "db_dumps/backups/benjaminelwyn-STAGING-" + grunt.template.today("dd-mm-yyyy") + ".sql"
      live:
        options:
          title: "Live DB"
          database: "<%= db_config.live.name %>"
          user: "<%= db_config.live.user %>"
          pass: "<%= db_config.live.pass %>"
          host: "<%= db_config.live.host %>"
          port: "<%= db_config.live.port %>"
          backup_to: "db_dumps/backups/benjaminelwyn-LIVE-" + grunt.template.today("dd-mm-yyyy") + ".sql"

    db_import:
      options: {}
      local:
        options:
          title: "Upload local DB"
          database: "<%= db_config.local.name %>"
          user: "<%= db_config.local.user %>"
          pass: "<%= db_config.local.pass %>"
          host: "<%= db_config.local.host %>"
          port: "<%= db_config.local.port %>"
          backup_to: "db_dumps/backups/benjaminelwyn-LIVE-" + grunt.template.today("dd-mm-yyyy") + ".sql"
      staging:
        options:
          title: "Upload staging DB"
          database: "<%= db_config.staging.name %>"
          user: "<%= db_config.staging.user %>"
          pass: "<%= db_config.staging.pass %>"
          host: "<%= db_config.staging.host %>"
          port: "<%= db_config.staging.port %>"
          backup_to: "db_dumps/backups/benjaminelwyn-LOCAL-" + grunt.template.today("dd-mm-yyyy") + ".sql"
      live:
        options:
          title: "Upload live DB"
          database: "<%= db_config.live.name %>"
          user: "<%= db_config.live.user %>"
          pass: "<%= db_config.live.pass %>"
          host: "<%= db_config.live.host %>"
          port: "<%= db_config.live.port %>"
          backup_to: "db_dumps/backups/benjaminelwyn-LOCAL-" + grunt.template.today("dd-mm-yyyy") + ".sql"


  
  # Load grunt plugins.
  grunt.loadNpmTasks "assemble"
  grunt.loadNpmTasks "grunt-contrib-compass"
  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-browser-sync"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-contrib-connect"
  grunt.loadNpmTasks "grunt-contrib-imageMin"
  grunt.loadNpmTasks "grunt-image-resize"
  grunt.loadNpmTasks "grunt-mysql-dump-import"
  grunt.loadNpmTasks "grunt-svgmin"
  grunt.loadNpmTasks "grunt-svg2png"

  
  #register tasks
  
  #watch with css inject
  grunt.registerTask "default", [
    # "connect"
    "browserSync"
    "watch"
  ]
  
  # compile all files that need compiling
  grunt.registerTask "c", [
    # "assemble"
    "compass"
    "coffee"
    "uglify:js_scripts"
    "uglify:plugins"
    "uglify:combine_all"
    "svgmin"
  ]

  grunt.registerTask "coff", ["coffee"]
  grunt.registerTask "js", ["uglify:js_scripts"]
  grunt.registerTask "sass", ["compass"]
  grunt.registerTask "ass", ["assemble"]
  grunt.registerTask "plug", ["uglify:plugins"]
  grunt.registerTask "min", ["imagemin"]

  # dump SQL
  grunt.registerTask 'dump', ['db_dump:local']
  grunt.registerTask 'dump_stage', ['db_dump:staging']
  grunt.registerTask 'dump_live', ['db_dump:live']
  # Upload SQL
  grunt.registerTask 'db_up_local', ['db_import:local']
  grunt.registerTask 'db_up_stage', ['db_import:staging']
  grunt.registerTask 'db_up_live', ['db_import:live']

  # minify SVGs
  grunt.registerTask "svg", ["svgmin"]

  # Convert SVG to PNG
  grunt.registerTask "png", ["svg2png"]