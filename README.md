# Assets for dk8 site development

- Compiles SASS with Compass
- Compiles CoffeeScript
- Merges plugins with JS files
- Minifies SVG files
- Creates a webfont with icons from SVGs
- Dumps DBs
- Uploads DBs
- Refreshes browser on markup changes
- Injects new CSS and images when they change
- Resizes and minifies images
- Creates static site with Assemble + Jade + markdown

## Grunt configuration

1. Set the path to the assets folder
2. Set the path to the public folder
3. Set the DB details in `db_dumps/db_dump_config.json`

#### If using Assemble...

4. Set path to Jade templates
5. Set path to YAML data files
6. Uncomment Assemble task in Gruntfile `grunt c` task