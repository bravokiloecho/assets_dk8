Routing = do ->

    # GLOBAL
    $window = null
    $body = null
    $wrapper = null

    # SPECIFIC TO HERE
    initialPage = true
    currentPage = null
    
    homePage = null
    aboutPage = null
    galleriesParent = null
    galleryPage = null

    # Defind in setupRouting
    homePath = null
    galleryPath = null
    aboutPath = null

    transitionInProgress = false
    pathToRun = false
    
    defineVars = ->
        gv = window.gv
        $window = gv.$window
        $body = gv.$body
        $wrapper = gv.$wrapper

        # Specific to here
        
        currentPage = ''
        currentSlug = window.currentSlug

        homePage = 'home'
        aboutPage = window.pathNames.about
        galleriesParent = window.pathNames.galleriesParent
        galleryPage = 'gallery'


    init = ->

        if !Modernizr.history
            return

        do defineVars
        do setupRouting
        

    # ROUTING USES DAVIS.JS
    setupRouting = ->
        
        homePath = '/?'
        galleryPath = '/' + galleriesParent
        aboutPath = '/' + aboutPage + '/?'

        # ROUTES
        app = Davis ->
            
            # HOME
            this.get homePath, ( r ) ->
                
                pageIn = homePage
                pageOut = currentPage
                
                # If transition in progress, store path and run at end
                if transitionInProgress
                    cachePath r.path
                    return

                # if same page, do nothing
                if pageIn is pageOut
                    console.log 'pageIn: '+pageIn
                    console.log 'pageOut: '+pageOut
                    console.log '*** SAME PAGE, STOP HERE ***********'
                    return

                switchPage pageOut, pageIn

            # ABOUT
            this.get aboutPath, ( r ) ->
                
                pageIn = aboutPage
                pageOut = currentPage
                
                # If transition in progress, store path and run at end
                if transitionInProgress
                    cachePath r.path
                    return

                # if same page, do nothing
                if pageIn is pageOut
                    console.log '*** SAME PAGE, STOP HERE ***********'
                    return

                args = 
                    requiresAjax: true

                switchPage pageOut, pageIn

            # GALLERY PAGE
            this.scope galleryPath, ( r ) ->

                # If transition in progress, store path and run at end
                if transitionInProgress
                    console.log 'TRANSITION IN PROGRESS'
                    cachePath r.path
                    return

                # Gallery without hash
                this.get '/:slug', ( r ) ->

                    pageIn = galleryPage
                    pageOut = currentPage
                    slug = r.params.slug
                    
                    # if same page, do nothing
                    if pageIn is pageOut and slug is currentSlug
                        console.log '*** SAME PAGE, STOP HERE ***********'
                        console.log 'pageIn: '+pageIn
                        console.log 'slugIn: '+slug
                        console.log 'pageOut: '+pageOut
                        console.log 'currentSlug: '+currentSlug
                        return

                    args = 
                        requiresAjax: true
                        pageType: pageIn
                        slug: slug

                    switchPage pageOut, pageIn, args

                # Gallery with hash
                this.get '/:slug/*splash', ( r ) ->

                    pageIn = galleryPage
                    pageOut = currentPage
                    slug = r.params.slug
                    hash = r.params.splash.replace '#/', ''
                    
                    # if same page, do nothing
                    if pageIn is pageOut and slug is currentSlug
                        console.log '*** SAME PAGE, STOP HERE ***********'
                        console.log 'pageIn: '+pageIn
                        console.log 'slugIn: '+slug
                        console.log 'pageOut: '+pageOut
                        console.log 'currentSlug: '+currentSlug
                        return

                    args = 
                        requiresAjax: true
                        pageType: pageIn
                        slug: slug

                    switchPage pageOut, pageIn, args


        app.configure ( config ) ->
            config.linkSelector = '.ajax'
            config.generateRequestOnPageLoad = true

        app.start()


    switchPage = ( pageOut, pageIn, args ) ->

        args = args || {}

        slug = args.slug
        requiresAjax = args.requiresAjax

        transitionInProgress = true
        transitionPromise = $.Deferred()

        console.log '*** RUN PATH ***'
        console.log 'pageOut: '+pageOut
        console.log 'pageIn: '+pageIn
        console.log 'transitionInProgress: '+transitionInProgress

        # Run transition
        runTransitionFunction pageOut, pageIn, slug, transitionPromise

        # At end of page transition, check whether
        # there are any pending paths
        # if so, run that path
        $.when( transitionPromise ).then ->
            
            if pathToRun
                console.log '!!!!!!! THERE IS A PATH TO RUN !!!!!!!'
                console.log 'PATH: ' + pathToRun
                
                # Set some variable before recalling the page transition
                transitionInProgress = false
                setFinalPageVars pageIn, slug
                
                runPath pathToRun

                # Reset path to run
                pathToRun = false
                return

            # At end, set vars
            console.log 'END OF PAGE TRANSITION'
            pathToRun = false
            transitionInProgress = false
            setFinalPageVars pageIn, slug

    runTransitionFunction = ( pageOut, pageIn, slug, promise ) ->

        # FOR INITIAL PAGE LOAD
        if initialPage
            PageTransition.initialSetup pageIn, slug, promise
            initialPage = false
            return
        
        # HOME => GALLERY
        if pageOut is homePage and pageIn is galleryPage
            console.log 'HOME => GALLERY' 
            PageTransition.homeToGallery slug, promise
            return

        # GALLERY => HOME
        if pageOut is galleryPage and pageIn is homePage
            console.log 'GALLERY => HOME' 
            PageTransition.galleryToHome promise
            return

        # Sub pages to sub pages...
        # #########################

        pagesInvolved = {}

        pagesInvolved.out = pageOut
        pagesInvolved.in = pageIn

        # PageTransition.subPagetoSubPage promise, pagesInvolved, slug

    # MISC ROUTING FUNCTIONS
    setFinalPageVars = ( pageIn, slug, transitionComplete = true ) ->
        
        currentPage = window.currentPage = pageIn
        currentSlug = window.currentSlug = slug

        $body
            .data 'page', currentPage
            .removeClass()
            .addClass currentPage

    cachePath = ( path ) ->
        pathToRun = path

    runPath = ( path ) ->
        Davis.location.assign new Davis.Request path

    goHome = ->
        runPath homePath.replace '?', ''

    # Export functions
    {
        init: init
        goHome: goHome
    }