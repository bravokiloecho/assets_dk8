PageTransition = do ->

    # GLOBAL
    $window = null
    $body = null
    $wrapper = null

    # SPECIFIC TO HERE
    siteURL = null
    currentPage = null
    
    homePage = null
    aboutPage = null
    galleriesParent = null
    galleryPage = null

    transitionInProgress = false
    pathToRun = false

    # TYPE OF GALLERY
    isDesktop = null
    
    defineVars = ->
        gv = window.gv
        $window = gv.$window
        $body = gv.$body
        $wrapper = gv.$wrapper

        # Specific to here
        siteURL = gv.siteURL
        currentPage = window.currentPage
        currentSlug = window.currentSlug

        homePage = 'home'
        aboutPage = window.pathNames.about
        galleriesParent = window.pathNames.galleriesParent
        galleryPage = 'gallery'

        # TYPE OF GALLERY
        isDesktop = gv.isDesktop

    init = ->
        do defineVars

    # INITIAL SETUP
    # Run this to setup initial page
    initialSetup = ( initialPage, slug, promise ) ->
        console.log 'initialSetup'
        console.log 'Page in: ' + initialPage
        console.log 'Slug: ' + slug

        pageReady = $.Deferred()

        # HOME PAGE
        if initialPage is homePage
            pageReady.resolve()
            $.when( pageReady ).then ->
                promise.resolve()

        # OTHER PAGES
        else

            # INITIAL GALLERY SETUP
            if initialPage is galleryPage

                # Do somethin then resolve pageReady promise

                $.when( pageReady ).then ->
                    promise.resolve()

                return


    # ROUTES
    # The routes are controlled here

    # HOME => GALLERY
    homeToGallery = ( slug, promise ) ->
        url = siteURL + '/' + galleriesParent + '/' + slug
        galleryLoaded = $.Deferred()
        galleryReady = $.Deferred()


        # Load gallery markup
        Load.page url, galleryLoaded
        $.when( galleryLoaded ).then ( $markup ) ->
            startGallery $markup, galleryReady

        $.when( galleryReady ).then ->
            # Do something
            promise.resolve()

    # GALLERY => HOME
    galleryToHome = ( promise ) ->
        galleryHidden = $.Deferred()

        # Do something and resolve promise

        $.when( galleryHidden ).then ->
            promise.resolve()


    # HELPERS
    # #######
            


    # EXPORT FUNCTIONS
    {
        init: init
        initialSetup: initialSetup
        homeToGallery: homeToGallery
        galleryToHome: galleryToHome
    }
