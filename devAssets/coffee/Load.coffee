Load = do ->

	# LOAD A GALLERY PAGE
	page = ( url, promise ) ->
		$.ajax url,
			cache: true
			dataType: 'html'
			success: ( markup ) ->
				$content = $(markup)
				promise.resolve( $content )
			error: ->
				# If fail, just redirect to the correct page
				window.location.href = url

	{
		page: page
	}