Controller = do ->
	
	# GLOBAL
	$window = null
	$body = null
	$wrapper = null
	WIDTH = null
	HEIGHT = null
	isMobile = null
	isTablet = null
	isDevice = null
	isTouch = null

	# SPECIFIC TO HERE


	defineVars = ->
		gv = window.gv
		$window = gv.$window
		$body = gv.$body
		$wrapper = gv.$wrapper
		WIDTH = gv.WIDTH
		HEIGHT = gv.HEIGHT

		isMobile = gv.deviceVariables.isMobile
		isTablet = gv.deviceVariables.isTablet
		isDevice = gv.deviceVariables.isDevice
		isTouch = gv.deviceVariables.isTouch

		# SPECIFIC TO HERE


	init = ->
		do defineVars

		
		do keyboardShortcuts
		

	
	# MISC FUNCTIONS
	# ##############

	keyboardShortcuts = ( $slider, $imagePanel ) ->
		if window.isDev
			Mousetrap.bind [ 't' ] , (e) ->
				do toggleHomeThumbs

			Mousetrap.bind [ 'l' ] , (e) ->
				do testLoad



	{
		init: init
	}